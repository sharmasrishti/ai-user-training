****SUMMARY  ****

**CONVOLUTIONAL NEURAL NETWORK**

**DEFINITION**

- CNN which is known as Convolutional Neural Network is a class of Deep Neural Network. It is ann application which used of classification and analyzing visual imagery.
- For example if we have differetiate between a bird and fish or a fruit or a vegetable we do that by looking at the features of the image and the features are used as filters.

**WORKING OF NETWORK**

- The whole process start by creating a filter which has different features like colour, size, shape and in case of bird and fish the feature can also be eyes, nose, foot and various other body parts.
- The whole process of applying the filter and convolution is done until we get a fine output which is good enough to classify the image.
- Then comes the activation function which helps to decide if the neuron would fire or not ie the neuron is active or not. There are many types of activation function the most commonly used is **Rectified Linear Unit (ReLU)**.
- After this feature maps which are the output containing the features above mentioned. this output we get from the convolution . So the feature maps are flattened so that they come in single vector which then becomes the first layer of deep neural network which is also known as the fully connected layer.
- At last we get an output layer which classifies the output and gives the probability of each class, for which normally **softmax** is used.

**IMPORTANT TERMS**

1. Kernel size (K): kernel size refers to the width x height of the filter.
2. Stride (S): stride is the number of pixels shifts over the input matrix. 
3. Zero padding (pad): it refers to the process of symmetrically adding zeroes to the input matrix.
4. Number of filters (F): it refers to the no. of neurons or to be more precise it is the neuron,s input weights form.
5. Flattening : it is converting the data into a 1 dimensional array for inputting it to the next layer.

