**SUMMARY**

Neural Network is a network of neurons or a circuit of neurons.

neural network ![image](uploads/cfee693b511b886b7a95d4fce4a9f670/image.png)

**DEEP NEURAL NETWORK**
dnn ![image](uploads/3563c56ae592923a8670cc4d649b45a3/image.png)

**DEFINITION**

  DNN which is known as ** Deep Neural Network**. It is an artificial neural network with multiple layers between input and output layers.

There are three layers in a DNN :
-  Input Layer
-  Hidden Layer
-  Output Layer

Every neuron has a specific **activation number**. **Weights** are assigned to each neuron of first layer. All the activations of the first layer are taken and then compute to the weighted sum of all.


activation funtion ![image](uploads/f97d4bb6cdd935c6664817d91055e3ed/image.png)

types of activation function ![image](uploads/017a3f441611b683eab1b674613e9ddd/image.png)

**SIGMOID FUNCTION**
  Sigmoid function also known as the logistic function.The input to the function is transformed into a value between 0.0 and 1.0. Before applyling     the  activation function we add bias (constant).
Bias neuron is a special neural added to each layer in the neural network.


But since , the commonly used activation function is ReLU and it is easy to use so we apply the respective function instead of sigmoid function. The range of the **ReLU function** between **0 and a** ie the activation number which is given , not necessary that it should be denoted by a it can be denoted by any variable.

ReLU function ![image](uploads/1bc8cfd04f31a6a175d123b6018a033b/image.png)

**GRADIENT DESCENT**

gradient descent ![image](uploads/37b4e0d9ee78d681d3a7c7629da256ba/image.png)

  Gradient descent is used to minimize the function by iteratively moving in the direction of **steepest descent** ie it tells the direction in which you should step to increase the function much faster.

**BACKPROPOGATION**

backpropogation ![image](uploads/39783eea99c4de083953803339b04cdf/image.png)

  The procedure of evaluating the expression of the derivative of the cost function as product of derivatives between each layer from left to right ie backwards. 
  It is basically an algorithm used for supervised learning of neural network using the gradient descent.

**STEPS**:

1. Initiliaze weights and biases in the network.
2. Propogate the inputs forward (by applyin the activation function).
3. Backpropogate the error (by updating weights and biases).
4. Terminating condition(when the error is very small).

 


  
 


